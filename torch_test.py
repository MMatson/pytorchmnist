import torch
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from torch.utils.data import Dataset

torch.manual_seed(1)

# x = torch.tensor(1.0, requires_grad=True)
# y = 2 * x ** 3 + x
# y.backward()
# print(x.grad)

class ex_set(Dataset):
    def __init__(self, length = 100, transform = None):
        self.x  = 2 * torch.ones(length, 2)
        self.y = torch.ones(length, 1)
        self.len = length
        self.transform = transform
    
    def __getitem__(self, index):
        sample = self.x[index], self.y[index]
        if self.transform:
            sample = self.transform(sample)
        return sample

    def __len__(self):
        return self.len

dataset = ex_set()

print("Ex. Set: ", )