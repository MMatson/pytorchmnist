import os
import sys

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.utils.data import DataLoader
import matplotlib.pylab as plt
import numpy as np

# torch.manual_seed(0)

data_root = './data'
if not os.path.exists(data_root):
    os.mkdir(data_root)

model_root = './models'
if not os.path.exists(model_root):
    os.mkdir(model_root)

batch_size = 100    
max_epochs = 3
learning_rate = 0.001

transform_list = transforms.Compose(
    [transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))]
)

training_set = datasets.MNIST(root=data_root, train=True, transform=transform_list, download=True)
validation_set = datasets.MNIST(root=data_root, train=False, transform=transform_list, download=True)

training_loader = DataLoader(dataset=training_set, batch_size=batch_size, shuffle=True)
validation_loader = DataLoader(dataset=validation_set, batch_size=batch_size, shuffle=False)

first_image, label = training_set[0]
# plt.imshow(first_image.numpy()[0], cmap = 'gray')
# plt.title(label)
# plt.show()


class SCNN(nn.Module):
    def __init__(self):
        super(SCNN, self).__init__()

        self.convolutional_layers = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=5),
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Conv2d(32, 64, kernel_size=5),
            nn.ReLU(),
            nn.MaxPool2d(2)
        )

        self.neural_layers = nn.Sequential(
            nn.Linear(64*4*4, 1024),
            nn.ReLU(),
            nn.Dropout(0.2),
            nn.Linear(1024, 10)
        )
        

    def forward(self, x):
        x = self.convolutional_layers(x)
        x = x.view(x.size(0), -1)
        x = self.neural_layers(x)
        return F.log_softmax(x, dim=1)


net = SCNN()

print(net)

optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)

val_loss_history = []
val_acc_history = []

train_loss_history = []
train_acc_history = []

batch_losses = []

def train(epoch):
    net.train()
    train_loss = 0
    correct = 0

    for batch_index, (image_data, target_data) in enumerate(training_loader):
        image = Variable(image_data)
        target = Variable(target_data)
        optimizer.zero_grad()
 
        output = net(image)
        loss = F.nll_loss(output, target)
        train_loss = loss.item()
        batch_losses.append(train_loss)
        
        prediction = output.data.max(1)[1]
        correct += prediction.eq(target.data).cpu().sum()
        
        loss.backward()
        optimizer.step()

        if batch_index % 100 == 0:
            print(train_loss)

    accuracy = 100.0 * correct / len(training_loader.dataset)
    train_acc_history.append(accuracy)

    

def validate(epoch):
    net.eval()
    test_loss = 0
    correct = 0

    for image_data, target_data in validation_loader:
        image = Variable(image_data)
        target = Variable(target_data)

        output = net(image)

        test_loss = F.nll_loss(output, target).item()
        prediction = output.data.max(1)[1]
        correct += prediction.eq(target.data).cpu().sum()
    
    test_loss /= len(validation_loader)
    val_loss_history.append(test_loss)
    accuracy = 100.0 * correct / len(validation_loader.dataset)
    val_acc_history.append(accuracy)

    print('\nTest Set: Average Loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)'.format(
        test_loss, correct, len(validation_loader.dataset),
        accuracy))

    print('\nSaving neural model to disk...'.format(epoch, max_epochs))
    model_path = os.path.join(model_root, 'scnn.pt')
    scripted_model = torch.jit.script(net)
    torch.jit.save(scripted_model, model_path)
    print('Model saved as: {}\n'.format(model_path))

    # sys.exit(0)

epoch_losses = []

for epoch in range(0, max_epochs):
    print("\nBeginning Epoch %d...\n" % epoch)
    train(epoch)
    validate(epoch)
    epoch_losses.append(val_loss_history[epoch])

batch_indices = []
for index in range(0, len(batch_losses)):
    batch_indices.append(index)

plt.plot(batch_indices, batch_losses, label="Batch Loss")
plt.show()

# epoch_indices = []
# for index in range(0, len(epoch_losses)):
#     epoch_indices.append(index)

# plt.plot(epoch_indices, epoch_losses)
# plt.show()


